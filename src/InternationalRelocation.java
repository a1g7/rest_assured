
public abstract class InternationalRelocation {

	public int age;

	// abstrcat methods
	public abstract void Visa();

	public abstract void Passport();

	// non abstract methods
	public void relocate() {
		System.out.println("Follow all rules of PR application");
	}
	/*
	 * public static void main(String[] args) { // TODO Auto-generated method stub
	 * 
	 * }
	 */

}
