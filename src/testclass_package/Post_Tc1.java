package testclass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_method_package.Trigger_API_Method;
import Request_Repository.Post_Request_Repository;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Post_Tc1 extends Trigger_API_Method {
	@Test
	public static void executor() throws IOException {
		String requestbody = Post_Tc1_Request();

		File DirectoryName = Handle_API_Logs.Create_log_Directory("Post_Tc1");

		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_Status_Code(requestbody, post_endpoint());
			 System.out.println(Status_Code);

			if (Status_Code == 201) {
				String response_Body = extract_response_Body(requestbody, post_endpoint());
				System.out.println(response_Body);
				Handle_API_Logs.evidence_creator(DirectoryName, "Post_Tc1", post_endpoint(), Post_Tc1_Request(),
						response_Body);
				validator(requestbody, response_Body);

				break;
			} else {
				//System.out.println("Desired Status Code not found hence retry");
			}

		}

	}

	public static void validator(String requestbody, String response_Body) throws IOException {
		// create an object of jsonpath to parse the requestbody
		JsonPath jsp_req = new JsonPath(requestbody);

		String req_name = jsp_req.getString("name");

		String req_job = jsp_req.getString("job");

		// create an object of jsonpath to parse the responsebody
		JsonPath jsp = new JsonPath(response_Body);

		String res_name = jsp.getString("name");
		System.out.println(res_name);
		
		String res_job = jsp.getString("job");
		System.out.println(res_job);

		String res_id = jsp.getString("id");
		System.out.println(res_id);

		String res_createdAt = jsp.getString("createdAt").substring(0, 10);

		LocalDateTime CurrentDate = LocalDateTime.now();
		//String ExpectedDate = CurrentDate.toString().substring(0, 10);

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		//Assert.assertEquals(res_createdAt, CurrentDate);

	}

}
