package testclass_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_method_package.Trigger_API_Method1;
import Request_Repository.Patch_Request_Repository;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Patch_TC1 extends Trigger_API_Method1 {
	@Test
	public static void executor() throws IOException {

		String requestbody = Patch_TC1_Request();

		File DirectoryName = Handle_API_Logs.Create_log_Directory("Patch_TC1");
		int Status_Code;

		for (int i = 0; i < 5; i++) {
			Status_Code = extract_Status_Code(requestbody, patch_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 201) {
				String response_Body = extract_response_Body(requestbody, patch_endpoint());
				 System.out.println(response_Body);
				 Handle_API_Logs.evidence_creator(DirectoryName, "Patch_TC1",patch_endpoint(), Patch_TC1_Request(), response_Body);
				validator(requestbody, response_Body);
				break;
			} else {
				//System.out.println("Desired Status Code not found hence, retry");
			}
			
		}
	}
	
	public static void validator(String requestbody, String response_Body) throws IOException {
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);

		JsonPath jsp = new JsonPath(response_Body);
		String res_name = jsp.getString("name");

		String res_job = jsp.getString("job");
		String res_updatedAt = jsp.getString("updatedAt");

		// create an object of json path to parse the requestbody
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		//Assert.assertEquals(res_updatedAt, ExpectedDate);

	}

}
