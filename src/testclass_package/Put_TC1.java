package testclass_package;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_method_package.Trigger_API_Method;
import Common_method_package.Trigger_API_Method2;
import Request_Repository.Put_Request_Repository;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Put_TC1 extends Trigger_API_Method2 {
	@Test
	public static void executor() throws IOException {
		
		String requestbody = Put_TC1_Request();
		
		File DirectoryName = Handle_API_Logs.Create_log_Directory("Put_TC1");
		int Status_Code;

		for (int i = 0; i < 5; i++) {
			Status_Code = extract_Status_Code(requestbody, put_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 200) {
				String response_Body = extract_response_Body(requestbody, put_endpoint());
				System.out.println(response_Body);
				Handle_API_Logs.evidence_creator(DirectoryName, "Put_TC1", put_endpoint(), Put_TC1_Request(),
						response_Body);
				validator(requestbody,response_Body);
				break;
			} else {
				//System.out.println("Desired status code not found hence, retry");
			}

		}
	}

	public static void validator(String requestbody,String response_Body) {

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp = new JsonPath(response_Body);
		String res_name = jsp.getString("name");

		String res_job = jsp.getString("job");

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

	}

}
