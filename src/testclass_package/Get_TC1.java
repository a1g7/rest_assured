 package testclass_package;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_method_package.Trigger_API_Get_Method;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;

public class Get_TC1 extends Trigger_API_Get_Method {
	@Test
	public static void executor() throws IOException {
		File DirectoryName = Handle_API_Logs.Create_log_Directory("Get_TC1");
		for (int i = 0; i < 5; i++) {
			int Status_Code = extract_Status_Code(get_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 200) {
				String response_Body = extract_response_Body(get_endpoint());
				System.out.println(response_Body);
				Handle_API_Logs.evidence_creator(DirectoryName, "Get_TC1", get_endpoint(), response_Body, response_Body);
				validator(response_Body);
				break;
			} else {
				//System.out.println("Desired Status Code not found hence retry");
			}

		}
	}

	public static void validator(String response_Body) {
		String[] Exp_id_Array = { "7", "8", "9", "10", "11", "12" };

		String[] Exp_email_Array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };

		String[] Exp_first_name_Array = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };

		String[] Exp_last_name_Array = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

		String[] Exp_avatar_Array = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };

		JsonPath jsp_res = new JsonPath(response_Body);

		String res_page = jsp_res.getString("page");

		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
		for (int i = 0; i < count; i++) {
			// array of id
			String Exp_id = Exp_id_Array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			// System.out.println(res_id);
			Assert.assertEquals(res_id, Exp_id);

			String Exp_email = Exp_email_Array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");
			// System.out.println(res_email);
			Assert.assertEquals(res_email, Exp_email);

			String Exp_first_name = Exp_first_name_Array[i];
			String res_first_name = jsp_res.getString("data[" + i + "].first_name");
			// System.out.println(res_first_name);
			Assert.assertEquals(res_first_name, Exp_first_name);

			String Exp_last_name = Exp_last_name_Array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			System.out.println(res_last_name);
			// Assert.assertEquals(res_last_name, Exp_last_name);

			String Exp_avatar = Exp_avatar_Array[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");
			System.out.println(res_avatar);
			Assert.assertEquals(res_avatar, Exp_avatar);

		}

	}

}
