import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;

public class dynamic_driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		// step 1 read the test cases to be executed from excel file
		
		ArrayList<String> TestCaseList = Excel_data_reader.Read_Excel_Data("API_data.xlsx", "TestCasesToExecute", "TestCaseToExecute");
		System.out.println(TestCaseList);
		int count = TestCaseList.size();
		
		for(int i=1; i<count; i++) {
			String TestCaseToExecute = TestCaseList.get(i);
			System.out.println("Test Case which is Going to be execute :" +TestCaseToExecute);
			
			//step 2 : Call the TestCaseToExecute on runtime by using java.lang.reflect package
			
			Class<?> TestClass = Class.forName("testclass_package."+TestCaseToExecute);
			
			//step 3 Call the execute method of the class captures in variable in test class by using java.lang.reflect.method
			
            Method ExecuteMethod = TestClass.getDeclaredMethod("executor+");

			
			//step 4 Set the accessibility of method as true
			
			ExecuteMethod.setAccessible(true);
			
			//step 5 Create the instance of class captured in test class variable
			
			Object InstanceOfTestClass = TestClass.getDeclaredConstructor().newInstance();
			
			//step 6 Execute the method captured in variable ExecuteMethod of class captured in TestClass variable
			
			ExecuteMethod.invoke(InstanceOfTestClass);
			System.out.println("Execution of test case name " +TestCaseToExecute +" is completed");
			System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			
		}
		
		 		}
		
		

	}


