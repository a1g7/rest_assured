import java.util.Calendar;

public class Exercises1 {

	public static void main(String[] args) {
		
		String str="Hello World";
		System.out.println("Hello".equalsIgnoreCase("HeLLO"));
		//System.out.println(str.endsWith("World"));
		//System.out.println(str.startsWith("Hello"));
		//System.out.println(str.split(" , "));
		
		//System.out.println(str.equals(str1));
		//System.out.println(str.toUpperCase());
		//System.out.println(str.replace("World", "Universe"));
		//String str1="welcome";
		//System.out.println(str.indexOf("World"));
		//System.out.println(str.substring(6));
		
		//System.out.println(str.concat(str1));
		
		//System.out.println(str.length());
		/*String originalStr="Java";
		String reversedStr="";
		System.out.println("Original String: " + originalStr);
		
		for(int i=0; i<originalStr.length(); i++) {
			reversedStr=originalStr.charAt(i)+reversedStr;
			
		}
		System.out.println("Reversed string: " + reversedStr);*/
		
       /* String str = "The quick brown fox jumps over the lazy dog.";

        // Get the index of all the characters of the alphabet
        // starting from the beginning of the String.
        int a = str.indexOf("a", 0);
        int b = str.indexOf("b", 0);
        int c = str.indexOf("c", 0);
        int d = str.indexOf("d", 0);
        int e = str.indexOf("e", 0);
        int f = str.indexOf("f", 0);
        int g = str.indexOf("g", 0);
        int h = str.indexOf("h", 0);
        int i = str.indexOf("i", 0);
        int j = str.indexOf("j", 0);
        int k = str.indexOf("k", 0);
        int l = str.indexOf("l", 0);
        int m = str.indexOf("m", 0);
        int n = str.indexOf("n", 0);
        int o = str.indexOf("o", 0);
        int p = str.indexOf("p", 0);
        int q = str.indexOf("q", 0);
        int r = str.indexOf("r", 0);
        int s = str.indexOf("s", 0);
        int t = str.indexOf("t", 0);
        int u = str.indexOf("u", 0);
        int v = str.indexOf("v", 0);
        int w = str.indexOf("w", 0);
        int x = str.indexOf("x", 0);
        int y = str.indexOf("y", 0);
        int z = str.indexOf("z", 0);
        
     // Display the results of all the indexOf method calls.
        System.out.println(" a  b c  d e  f  g h i  j");
        System.out.println("=========================");
        System.out.println(a + " " + b + " " + c + " " + d + " " +
                           e + " " + f + " " + g + " " + h + " " +
                           i + " " + j + "\n");

        System.out.println("k  l  m  n  o  p q  r  s  t");
        System.out.println("===========================");
        System.out.println(k + " " + l + " " + m + " " + n + " " +
                           o + " " + p + " " + q + " " + r + " " +
                           s + " " + t + "\n");

        System.out.println("u  v  w  x  y  z");
        System.out.println("================");
        System.out.println(u + " " + v + " " + w + " " + x + " " +
                           y + " " + z);*/



		/*String str = "This is a sample string.";

        // Copy the contents of the String to a byte array.
        // Only copy characters 4 through 10 from str.
        // Fill the source array starting at position 2.
        char[] arr = new char[] { ' ', ' ', ' ', ' ',
                                  ' ', ' ', ' ', ' ' };
        str.getChars(4, 10, arr, 2);

        // Display the contents of the byte array.
        System.out.println("The char array equals \"" +
            arr + "\"");*/
        
        
        
        
        
        
		/*String str = "This is a sample String.";

        // Copy the contents of the String to a byte array.
        byte[] byte_arr = str.getBytes();

        // Create a new String using the contents of the byte array.
        String new_str = new String(byte_arr);

        // Display the contents of the byte array.
        System.out.println("\nThe new String equals " +
            new_str + "\n");*/
        
        
        
        
        
        
	   /* Calendar c = Calendar.getInstance();
	      System.out.println("Current Date and Time :"); 
	      System.out.format("%tB %te, %tY%n", c, c, c);
	      System.out.format("%tl:%tM %tp%n", c, c, c); */


        /*String columnist1 = "Stephen Edwin King";
        String columnist2 = "Walter Winchell";
        String columnist3 = "Mike Royko";

        // Are any of the above Strings equal to one another?
        boolean equals1 = columnist1.equals(columnist2);
        boolean equals2 = columnist1.equals(columnist3);

        // Display the results of the equality checks.
        System.out.println("\"" + columnist1 + "\" equals \"" +
            columnist2 + "\"? " + equals1);
        System.out.println("\"" + columnist1 + "\" equals \"" +
            columnist3 + "\"? " + equals2);*/


       /* String str1 = "Python Exercises";
        String str2 = "Python Exercise";

        // The String to check the above two Strings to see
        // if they end with this value (se).
        String end_str = "se";

        // Check first two Strings end with end_str
        boolean ends1 = str1.endsWith(end_str);
        boolean ends2 = str2.endsWith(end_str);

        // Display the results of the endsWith calls.
        System.out.println("\"" + str1 + "\" ends with " +
            "\"" + end_str + "\"? " + ends1);
        System.out.println("\"" + str2 + "\" ends with " +
            "\"" + end_str + "\"? " + ends2);*/
		
		// Character array with data.
        /*char[] arr_num = new char[] { '1', '2', '3', '4' };

        // Create a String containig the contents of arr_num
        // starting at index 1 for length 2.
        String str = String.copyValueOf(arr_num, 1, 3);

        // Display the results of the new String.
        System.out.println("\nThe book contains " + str +" pages.\n");*/

        
        
	    /*String str1 = "example.com", str2 = "Example.com";
	    StringBuffer strbuf = new StringBuffer(str1);
	    
	    System.out.println("Comparing "+str1+" and "+strbuf+": " + str1.contentEquals(strbuf));
	    
	    System.out.println("Comparing "+str2+" and "+strbuf+": " + str2.contentEquals(strbuf));*/

	    
	    
	    
	    
	   /* String str1 = "example.com", str2 = "Example.com";
	    CharSequence cs = "example.com";
	     System.out.println("Comparing "+str1+" and "+cs+": " + str1.contentEquals(cs));
	      System.out.println("Comparing "+str2+" and "+cs+": " + str2.contentEquals(cs));*/

	      
	      
	      
	      
		/*String str1="PHP Exercises and Python Exercises";
		String str2="and";
		System.out.println("Original String: " + str1);
		System.out.println("Specifies sequence od char values: " + str2);
		System.out.println(str1.contains(str2));*/
		
		/*String str1="PHP Excercises and ";
		String str2="Python Exercises";
		System.out.println("String 1 : " + str1);
		System.out.println("String 2 : " + str2);
		//concanate the two strings
		String str3=str1.concat(str2);
		System.out.println("The concatenated String: " + str3);*/
		
		
		
		
		/*String str1="This is Exercise 1";
		String str2="This is Exercise 2";
		
		System.out.println("String 1 : " + str1);
		System.out.println("String 2 : " + str2);
		
		int result=str1.compareTo(str2);
		//display the result of comparision
		if(result < 0) {
			System.out.println("\""+str1+"\""+"is less than"+"\""+str2+"\"");
		}
		else if(result == 0)
		{
			System.out.println("\""+str1+"\""+"is equal to"+"\""+str2+"\"");
		}
		else //if(result > 0)
		{
			System.out.println("\""+str1+"\""+"is greater than"+"\""+str2+"\"");
		}*/
		
		
		/*String str="w3resource.com";
		System.out.println("Original String : " + str);
		
		int ctr=str.codePointCount(1,10);
		System.out.println("Codepoint count : " + ctr);*/
		
				
		/*String str="w3resource.com";
		System.out.println("Original String1 : " + str);
		
		int val1 =str.codePointAt(1);
		int val2=str.codePointAt(9);
		System.out.println("Character(unicode point) = " +val1);
		System.out.println("Character(unicode point) = " + val2);*/
		
		
		/*String str = "Java Exercises!";
		System.out.println("Original String =" + str);

		int index1 = str.charAt(0);
		int index2 = str.charAt(10);

		System.out.println("The character at position 0 is " + (char) index1);
		System.out.println("The character at position 10 is " + (char) index2);*/
		
		

	}

}
