import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_Reference {

	public static void main(String[] args) {

		// step 1:Declare the variables for baseURI
		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		// step 2:Declare the BaseURI
		RestAssured.baseURI = BaseURI;

		// step 3:configure the request body and trigger the API
		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.post("api/users").then().log().all().extract().response().asString();
		System.out.println(ResponseBody);

		// create an object of json path to parse the response body
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		System.out.println(res_job);

		String res_id = jsp_res.getString("id");
		System.out.println(res_id);

		String res_createdAt = jsp_res.getString("createdAt");
		System.out.println(res_createdAt);

		// parsing for date
		String mydate = res_createdAt.substring(0, 15);
		System.out.println(mydate);

		LocalDateTime CurrentDate = LocalDateTime.now();
		System.out.println(CurrentDate);

		// create an object of json path to parse the request body
		JsonPath jsp_req = new JsonPath(RequestBody);
		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		System.out.println(req_job);

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);

	}

}
