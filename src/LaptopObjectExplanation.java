
public class LaptopObjectExplanation {

	public static void main(String[] args) {
		LaptopExplanation myLaptop = new LaptopExplanation();
		myLaptop.company = "Intel";
		myLaptop.model = "HP";
		myLaptop.startLaptop();
		myLaptop.shutdownLaptop();

	}

}
