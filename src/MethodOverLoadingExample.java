
public class MethodOverLoadingExample {

	public void moveSpeed(String car) {
		System.out.println("Slow");

	}

	public void moveSpeed(double car) {
		System.out.println("Fast");
	}

	public static void main(String[] args) {

		MethodOverLoadingExample overLoad = new MethodOverLoadingExample();
		overLoad.moveSpeed("Kia");
		overLoad.moveSpeed(1);

	}

}
