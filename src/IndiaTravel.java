
public class IndiaTravel implements InternationalTravelRequirement, WorkTravelRequirement {

	@Override
	public void workVisa() {
		System.out.println("Work Visa Needed");

	}

	@Override
	public void visa() {
		System.out.println("Travel or Work Visa Needed");

	}

	@Override
	public void passport() {
		System.out.println("Passport with validity of 6 months needed");

	}

	// Native method to this class
	public void bankBalance() {
		System.out.println("5 lakhs INR balance needed");
	}

	public static void main(String[] args) {
		InternationalTravelRequirement myVisa = new IndiaTravel();
		myVisa.visa();
		myVisa.passport();
	}
}
