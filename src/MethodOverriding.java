
public class MethodOverriding extends ClassExplanation {

	// applyBrake method of class ClassExplanation is overriden in this class
	public void applyBrake() {
		System.out.println("Car stoopped after brakes applied");
	}

	public static void main(String[] args) {
		MethodOverriding override = new MethodOverriding();
		override.company = "Toyota";
		override.model = "Camery";
		override.applyBrake();

	}

}
