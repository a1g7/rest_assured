package common_utility_package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Logs {
	public static File Create_log_Directory(String DirectoryName) {
		String Current_Project_Directory = System.getProperty("user.dir");
		System.out.println(Current_Project_Directory);
		File Log_Directory = new File(Current_Project_Directory + "//API_Logs//" + DirectoryName);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir();
		return Log_Directory;

	}

	public static boolean Delete_Directory(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {

			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						Delete_Directory(file);
					} else {
						file.delete();

					}
				}
			}
			Directory_deleted = Directory.delete();
		} else {
			System.out.println("Directory does not exist");
		}
		return Directory_deleted;

	}

	public static void evidence_creator(File DirectoryName, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {
		// step 1:create the file at given location
		File newfile = new File(DirectoryName + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence:" + newfile.getName());

		// Step 2:write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint:" + endpoint + "\n\n");
		datawriter.write("RequestBody:\n\n" + requestBody + "\n\n");
		datawriter.write("ResponseBody:\n\n" + responseBody + "\n\n");
		datawriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE:" + newfile.getName());

	}

}
