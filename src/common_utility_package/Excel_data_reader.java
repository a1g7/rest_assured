package common_utility_package;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_reader {
	public static ArrayList<String> Read_Excel_Data(String File_name, String Sheet_name, String Test_Case_Name)
			throws IOException {

		ArrayList<String> ArrayData = new ArrayList<String>();

		// step1 locate the file

		String Project_Dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(Project_Dir + "\\Input_data\\" + File_name);

		// step 2 access the located excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// step3 count the number of sheets available in the excel file
		int countofsheet = wb.getNumberOfSheets();
		System.out.println(countofsheet);

		// step 4 access the desired sheet

		for (int i = 0; i < countofsheet; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				System.out.println("inside the sheet :" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentRow = Rows.next();

					// step 5 access the row corresponding to desired test case
					if (currentRow.getCell(0).getStringCellValue().equals(Test_Case_Name)) {

						java.util.Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
							// System.out.println(Data);
							ArrayData.add(Data);

						}
					}

				}
			}
		}
		wb.close();
		return ArrayData;

	}

}


	

	


