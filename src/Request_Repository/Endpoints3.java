package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;

public class Endpoints3 {

	static String hostname = "https://reqres.in/";

	public static String get_endpoint() throws IOException {
		ArrayList<String> excel_data = Excel_data_reader.Read_Excel_Data("API_data.xlsx", "Get_API", "Get_TC_1");
		System.out.println(excel_data);
		
		String URL = hostname + "api/users?page=2";
		System.out.println(URL);

		return URL;

	}

}
