package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;

public class Patch_Request_Repository extends Endpoints1 {

	public static String Patch_TC1_Request() throws IOException {

		ArrayList<String> excel_data = Excel_data_reader.Read_Excel_Data("API_data.xlsx", "Patch_API", "Patch_TC_1");
		 System.out.println(excel_data);
		 
		String req_name = excel_data.get(1);
		
		String req_job = excel_data.get(2);
		
		String RequestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";				

		return RequestBody;

	}

}
