import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Put_Reference {

	public static void main(String[] args) {

		// step 1:Declare the variables for baseURI
		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";

		// step 2:Declare the BaseURI
		RestAssured.baseURI = BaseURI;

		// step 3:Configure the requestbody and trigger the API
		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.put("api/users/2").then().extract().response().asString();
		System.out.println(ResponseBody);

		// create an object of json path to parse the responsebody
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		System.out.println(res_name);

		String res_job = jsp_res.getString("job");
		System.out.println(res_job);

		String res_updatedAt = jsp_res.getString("updatedAt");
		System.out.println(res_updatedAt);

		// create an object of json path to parse the requestbody
		JsonPath jsp_req = new JsonPath(RequestBody);
		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		System.out.println(req_job);

		String req_updatedAt = jsp_req.getString("updatedAt");
		System.out.println(req_updatedAt);

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);


	}

}
