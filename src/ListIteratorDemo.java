import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorDemo {

	public static void main(String[] args) {
		LinkedList l = new LinkedList();
		l.add("Abhi");
		l.add("Aishu");
		l.add("Chetan");
		l.add("Prajakta");
		System.out.println("Original LinkedList:" + l);
		
		//converting linkedlist to listoperator by using listiterator method
		ListIterator ltr=l.listIterator();
		
		//looping over list iterator
		while(ltr.hasNext()) {
			String s=(String) ltr.next();
			if(s.equals("Aishu")) {
				ltr.remove();
			}
			else if (s.equals("Prajakta")) {
				ltr.add("Piyush");
			}
			else if(s.equals("Chetan")) {
				ltr.set("Jyoti");
			}
		}
		System.out.println("Updated LinkedList:" +l);
		

	}
	

}
