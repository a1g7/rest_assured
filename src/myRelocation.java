
public class myRelocation extends InternationalRelocation {
	@Override
	public void Visa() {
		System.out.println("Visa not needed for relocation");
	}

	@Override
	public void Passport() {
		System.out.println("Passport with validity of 1 year needed");
	}

	public static void main(String[] args) {
		InternationalRelocation relocation = new myRelocation();
		relocation.age = 20;
		relocation.Visa();
		relocation.Passport();
		relocation.relocate();

	}

}
