import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListDemo {

	public static void main(String[] argst) {
		 ArrayList arrayList = new ArrayList<>();// default size 12

		 List list = new ArrayList<>();

		 list.add(1);
		 list.add(2);
		 list.add(2);
		 list.add(3);
		 list.add(4);
		 list.add(5);
		 list.add(6);
		 list.add(7);
		 list.add(8);
		 list.add(9);
		 list.add(10);
		list.add(11);

		 list.add(12);// again default size 12

		// System.out.println(list);
		 System.out.println(list.size());
		 System.out.println(list.get(6));

		// list.add(5, 1000);
		// System.out.println(list);

		// List<String> list = new ArrayList();

		// list.add("aishu");
		// list.add("6");
		// list.add("");
		// list.add("6");
		// System.out.println(list);

		// String a = list.get(1);
		// System.out.println(1+1+a+"aishu");
		// System.out.println(1+1+a);
		// System.out.println(a+1+1);
		// System.out.println(a+"aishu");
		// System.out.println(a.concat("ABHI"));

		// String str="java";
		// str +="programming";
		// str=str+"programming";

		// System.out.println(str);
		// System.out.println("this is a loop");

		// for(int i=0; i<5; i++) {
		// str +="programming";
		// System.out.println(str);
		// }

		List list1 = new LinkedList();
		list1.add(2);
		list1.add(4);
		list1.add(6);
		list1.add(10);
		list1.add(12);
		list1.add(14);
		list1.add("String");

		// list.add(3, 8);
		// list.add(7, 16);
		// list.add(3, new Character('@'));
		// List list1 = new LinkedList();
		// list1.add(16);
		// list1.add(18);
		// list1.add(20);
		// list1.add(7, 1);
		// list.remove(1);
		// list.remove(new character('1'));
		// System.out.println(list1);
		// list1.addAll(list1);
		// System.out.println(list1.contains(2));
		// System.out.println(" : "list);
		// System.out.println(list);
		// System.out.println("1st list :" + list);
		// System.out.println("2nd list :" + list1);
		// Boolean c = m1();
		// System.out.println(c);

		// }

		// public static Boolean m1() {
		// Boolean b;
		// if (2 == 2) {
		// b = true;
		// }
		// return b;
		// }
		//String name = "Aishu";
		//String name1 = new String("String");

		//list.remove(new String("String"));
		//System.out.println(list);

		//List list1 = new LinkedList();
		//list1.add(16);
		//list1.add(18);
		//list1.add(20);

		//list.remove(new Character('1'));
		//System.out.println(list);
		//list.addAll(list1);
		
		
		//System.out.println("1st list:"+list);
		//System.out.println("2nd list:"+list1);
		//list.removeAll(list1);
		//list.add(3,'@');
		//list.get(3);
		
		LinkedList<String> cars=new LinkedList<String>();
		cars.add("Volvo");
		cars.add("BMW");
		cars.add("Ford");
		
		cars.add("Ertiga");
		System.out.println(cars);
		
		cars.addFirst("Maruti");
		
		cars.addLast("Nexon");
		System.out.println(cars);
		
		
		cars.removeFirst();
		System.out.println(cars);
		
		
		cars.removeLast();
		System.out.println(cars);
		
		System.out.println(cars.getFirst());
		System.out.println(cars.getLast());
		
		String mystr="Abhishek";
		String mystr1="";
		System.out.println(mystr.isEmpty());
		System.out.println(mystr1.isEmpty());
		System.out.println(mystr.isBlank());
		System.out.println(mystr1.isBlank());
		
		String myStr="Hello planet earth,you are a great planet";
		System.out.println(myStr.indexOf("planet"));
		
		//const letters=new Set("a","b","c","d");
		
	
		
	

	}
}
