
public class ClassExplanation {

	String company;
	String model;

	public void startEngine() {
		System.out.println("Engine started for car :" + company + "and model :" + model);
	}

	public void applyBrake() {
		System.out.println("Brakes applied :" + company + "and model :" + model);
	}

}
