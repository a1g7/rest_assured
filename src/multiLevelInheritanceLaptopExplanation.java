
public class multiLevelInheritanceLaptopExplanation extends InheritanceLaptopExplanation {

	// Attribute created in this sub class
	String LaptopType;

	// Method created in this sub class
	public void RAM() {
		System.out.println("Laptop of :" + company + "and model :" + model + "has" + LaptopType + "RAM");
	}

	public static void main(String[] args) {
		multiLevelInheritanceLaptopExplanation typeOfLaptop = new multiLevelInheritanceLaptopExplanation();
		// Attributes inherited from superclass named LaptopExplanation
		typeOfLaptop.company = "Intel";
		typeOfLaptop.model = "HP";
		// Methods inherited from super class named LaptopExplanation
		typeOfLaptop.startLaptop();
		typeOfLaptop.shutdownLaptop();
		// Methods inherited from super class named InheritanceLaptopExplanation
		typeOfLaptop.processer();
		// Attribute created in this sub class is used
		typeOfLaptop.LaptopType = "Standarad";
		// Method created in this sub class is used
		typeOfLaptop.RAM();

	}

}
