  package Common_method_package;

import static io.restassured.RestAssured.given;

import Request_Repository.Endpoints3;
import testclass_package.Get_TC1;

public class Trigger_API_Get_Method extends Endpoints3 {

	public static int extract_Status_Code(String URL) {
		int StatusCode = given()
				.when().get(URL)
				.then().extract().statusCode();
		return StatusCode;
	}

	public static String extract_response_Body(String URL) {
		String ResponseBody = given()
				.when().get(URL)
				.then().extract().response().asString();
		return ResponseBody;
	}

}
