package Common_method_package;

import static io.restassured.RestAssured.given;

import Request_Repository.Put_Request_Repository;

public class Trigger_API_Method2 extends Put_Request_Repository {

	public static int extract_Status_Code(String req_Body, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(req_Body).when().put(URL).then()
				.extract().statusCode();
		return StatusCode;
	}

	public static String extract_response_Body(String req_Body, String URL) {
		String ResponseBody = given().header("Content-Type", "application/json").body(req_Body).when().put(URL).then()
				.extract().response().asString();
		return ResponseBody;

	}

}
