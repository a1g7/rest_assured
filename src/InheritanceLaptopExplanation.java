
public class InheritanceLaptopExplanation extends LaptopExplanation {

	// method created in this sub class
	public void processer() {
		System.out.println("Laptop of :" + company + "and model :" + model + "is able to process");
	}

	public static void main(String[] args) {
		InheritanceLaptopExplanation proLaptop = new InheritanceLaptopExplanation();
		// Attributes created created in this sub class
		proLaptop.company = "Intel";
		proLaptop.model = "HP";
		// Method created in this sub class
		proLaptop.startLaptop();
		proLaptop.shutdownLaptop();
		// method created in this sub class is used
		proLaptop.processer();

	}

}
