
public class InheritanceExplanation extends ClassExplanation {

	// Method created in this sub class
	public void accelerate() {
		System.out.println("Car of :" + company + "and model :" + model + "is able to accelerate");
	}

	public static void main(String[] args) {
		InheritanceExplanation accCar = new InheritanceExplanation();
		// Attributes inherited from super class named ClassExplanation
		accCar.company = "Maruti";
		accCar.model = "Ertiga";
		// Methods inherited from super class named ClassExplanation
		accCar.startEngine();
		accCar.applyBrake();
		// Method created in this sub class is used
		accCar.accelerate();

	}

}
