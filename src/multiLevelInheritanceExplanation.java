
public class multiLevelInheritanceExplanation extends InheritanceExplanation {

	// Attributes created in this sub class
	String gearBoxType;

	// Method created in this sub class
	public void gearBox() {
		System.out.println("Car of :" + company + "and model :" + model + "has" + gearBoxType + "gear box");
	}

	public static void main(String[] args) {
		multiLevelInheritanceExplanation gearOfCar = new multiLevelInheritanceExplanation();
		// Attributes inherited from super class named ClassExplanation
		gearOfCar.company = "Maruti";
		gearOfCar.model = "Ertiga";
		// Methods inherited from super class named ClassExplanation
		gearOfCar.startEngine();
		gearOfCar.applyBrake();
		// Methods inherited from super class named InheritanceExplanation
		gearOfCar.accelerate();
		// Attribute created in this sub class is used
		gearOfCar.gearBoxType = "Automatic";
		// Method created in this sub class is used
		gearOfCar.gearBox();

	}

}
