import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

public class Get_Reference {

	public static void main(String[] args, String ResponseBody) {

		// step 1:Declare the variables for BaseURI
		String BaseURI = "https://reqres.in/";

		// step 2:Declare the BaseURI
		RestAssured.baseURI = BaseURI;

		// step 3:trigger the API
		given().header("Content-Type", "application/json").log().all().when().get("api/users?page=2").then().log().all()
				.extract().response().asString();

		// create an object of json path to parse the response body
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);

		String res_email = jsp_res.getString("email");
		System.out.println(res_email);

		String res_first_name = jsp_res.getString("first_name");
		System.out.println(res_first_name);

		String res_last_name = jsp_res.getString("last_name");
		System.out.println(res_last_name);

		String res_avatar = jsp_res.getString("avatar");
		System.out.println(res_avatar);

	}

}
