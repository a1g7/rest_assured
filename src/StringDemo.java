import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class StringDemo {

	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap();
		map.put(111, "Aishu");
		map.put(222, "chetu");
		map.put(333, "abhi");
		map.put(444, "lingu");
//		System.out.println(map.put(111, "sulu"));
		System.out.println(map);
//		System.out.println(map.get(333));
//		System.out.println(map.containsKey(map));
//		System.out.println(map.remove(222));
//		System.out.println(map);
//		System.out.println(map.size());
//	map.clear();
//	System.out.println(map);
	    System.out.println(map.keySet());
	    System.out.println(map.isEmpty());
	    System.out.println(map.entrySet());
	    
	    Set s = map.keySet();
	    System.out.println("set of keys :" +s);
	    Collection<String> s1 = map.values();
	    System.out.println("group of values:" +s1);
	    
	    
	    
	    
	    
	    
	    
	    Set s2 = map.entrySet();
	    Iterator itr = s2.iterator();
	    while(itr.hasNext()) {
	    	Object o = itr.next();
	    	System.out.println("object :" +o);
	    }
		
		
		
		
		
		
		
		
//		TreeSet<String> set = new TreeSet<>();
//		set.add("Aishu");
//		set.add("lingu");
//		set.add("chetu");
//		set.add("abhi");
//		set.add("sulu");
//		set.add("Trisha");
//		set.add("Kratik");
//		System.out.println(set);
//		//System.out.println(set.first());
//		//System.out.println(set.last());
//		//System.out.println(set.headSet(300));
//		//System.out.println(set.tailSet(600));
//		//System.out.println(set.subSet(200,300));
//		System.out.println(set.comparator());
//		//System.out.println(set.floor(200));
//		//System.out.println(set.ceiling(300));
//		//System.out.println(set.descendingSet());
		
		
		
		
		
		
//		Set s= new HashSet();
//		s.add("Apple");
//		s.add("WaterMelon");
//		s.add("Banana");
//		s.add("Orange");
//		s.add("Jackfruit");
//		s.add("Mango");
//		System.out.println(s);
//		
		
		}
	}






/*
 * String Statement = "hello"; System.out.println("String length" +
 * Statement.length());
 * 
 * String[] str = Statement.split(","); int i = str.length;
 * System.out.println("String count:" + i);
 * 
 * System.out.println(Statement.equalsIgnoreCase("HELLO"));
 */

/*
 * String s1 = "jhon";
 * 
 * String s2 = new String("jhon");
 * 
 * String s3 = "jhon";
 * 
 * String s4 = new String("jhon");
 * 
 * System.out.println("1st case" + s1.equals(s3));
 * 
 * System.out.println("2nd case" + s1.equals(s2));
 * 
 * System.out.println("3rd case" + s1 == s2);
 * 
 * String Statement = "hello this is java tutorial";
 * System.out.println("String length" + Statement.length());
 * 
 * String[] str = Statement.split(","); int i = str.length;
 * System.out.println("String count:" + i);
 * 
 * System.out.println(Statement.equalsIgnoreCase("HELLO"));
 */

/*
 * String firstname = "Aishu"; String lastname = "Nilange";
 * System.out.println(firstname + "" + lastname);
 */

/*
 * String firstName = "Aishu"; String lastName = "Nilange";
 * System.out.println(firstName.concat(lastName));
 */

/*
 * List<String> ls = new ArrayList<String>(); ls.add("bill Utility");
 * ls.add("Electricity"); ls.add("Recharge"); ls.add("Water bill");
 * System.out.println(ls);
 * 
 * List<String> ls1 = new ArrayList<String>(); ls1.add("Jio");
 * ls1.add("Airtel"); ls1.add("VI"); ls1.add("BSNL"); System.out.println(ls1);
 * 
 * Map<String, Object> hs = new HashMap(); hs.put("Services", ls);
 * hs.put("Operators", ls1); System.out.println(hs);
 * 
 * List<Object> list = (List<Object>) hs.get("Operators");
 * System.out.println(list.get(0)); System.out.println(list.get(1));
 * System.out.println(list.get(2)); System.out.println(list.get(3));
 */
