import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;
import common_utility_package.Handle_API_Logs;
import testclass_package.Get_TC1;

import testclass_package.Patch_TC1;
import testclass_package.Post_Tc1;
import testclass_package.Put_TC1;
import testclass_package.Post_Tc1;

public class Driver_Class {

	public static void main(String[] args) throws IOException {

		Post_Tc1.executor();
		System.out.println("---------------------------------------");

		Patch_TC1.executor();
		System.out.println("---------------------------------------");

		 Put_TC1.executor();
		 System.out.println("---------------------------------------");

		 Get_TC1.executor();
		 System.out.println("---------------------------------------");

		// Handle_API_Logs.Create_log_Directory("logs");

	}

}
